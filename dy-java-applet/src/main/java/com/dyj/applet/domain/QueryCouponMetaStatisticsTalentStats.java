package com.dyj.applet.domain;

/**
 * 查询券模板发放统计数据 主播发放
 */
public class QueryCouponMetaStatisticsTalentStats {

    /**
     * <p>通过主播领取对应券模板的核销数量</p>
     */
    private Long consumed_num;
    /**
     * <p>主播发券期间对应券模板的曝光数</p>
     */
    private Long exposed_num;
    /**
     * <p>主播发券期间对应券模板的领取数</p>
     */
    private Long received_num;
    /**
     * <p>主播抖音号</p> 选填
     */
    private String talent_account;
    /**
     * <p>主播open_id（主播在开发者小程序内的open_id）</p>
     */
    private String talent_open_id;

    public Long getConsumed_num() {
        return consumed_num;
    }

    public QueryCouponMetaStatisticsTalentStats setConsumed_num(Long consumed_num) {
        this.consumed_num = consumed_num;
        return this;
    }

    public Long getExposed_num() {
        return exposed_num;
    }

    public QueryCouponMetaStatisticsTalentStats setExposed_num(Long exposed_num) {
        this.exposed_num = exposed_num;
        return this;
    }

    public Long getReceived_num() {
        return received_num;
    }

    public QueryCouponMetaStatisticsTalentStats setReceived_num(Long received_num) {
        this.received_num = received_num;
        return this;
    }

    public String getTalent_account() {
        return talent_account;
    }

    public QueryCouponMetaStatisticsTalentStats setTalent_account(String talent_account) {
        this.talent_account = talent_account;
        return this;
    }

    public String getTalent_open_id() {
        return talent_open_id;
    }

    public QueryCouponMetaStatisticsTalentStats setTalent_open_id(String talent_open_id) {
        this.talent_open_id = talent_open_id;
        return this;
    }
}
