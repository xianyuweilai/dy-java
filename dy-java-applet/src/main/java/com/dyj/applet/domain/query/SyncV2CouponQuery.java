package com.dyj.applet.domain.query;

import com.dyj.applet.domain.CouponAttribute;
import com.dyj.applet.domain.CouponAvailableInfo;
import com.dyj.applet.domain.SpuEntryInfo;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-06 16:29
 **/
public class SyncV2CouponQuery extends BaseQuery {

    /**
     * 同步行为 1-上线，2-下线
     */
    private Integer status;
    /**
     * 	优惠券属性
     */
    private CouponAttribute attribute;

    /**
     * 领取结束时间（可选）（10位）unixtime 秒级别
     */
    private Long claim_end_time;

    /**
     * 领取开始时间（可选）unixtime 秒级别
     */
    private Long claim_start_time;
    /**
     * 	外部优惠券ID
     */
    private String ext_coupon_id;

    /**
     * 可用列表
     */
    private List<CouponAvailableInfo> available_info;
    /**
     * 小程序入口参数
     */
    private SpuEntryInfo entry_info;


    public static SyncV2CouponQueryBuilder builder() {
        return new SyncV2CouponQueryBuilder();
    }

    public static class SyncV2CouponQueryBuilder {
        private Integer status;
        private CouponAttribute attribute;
        private Long claimEndTime;
        private Long claimStartTime;
        private String extCouponId;
        private List<CouponAvailableInfo> availableInfo;
        private SpuEntryInfo entryInfo;
        private Integer tanantId;
        private String clientKey;
        public SyncV2CouponQueryBuilder status(Integer status) {
            this.status = status;
            return this;
        }
        public SyncV2CouponQueryBuilder attribute(CouponAttribute attribute) {
            this.attribute = attribute;
            return this;
        }

        public SyncV2CouponQueryBuilder claimEndTime(Long claimEndTime) {
            this.claimEndTime = claimEndTime;
            return this;
        }
        public SyncV2CouponQueryBuilder claimStartTime(Long claimStartTime) {
            this.claimStartTime = claimStartTime;
            return this;
        }
        public SyncV2CouponQueryBuilder extCouponId(String extCouponId) {
            this.extCouponId = extCouponId;
            return this;
        }
        public SyncV2CouponQueryBuilder availableInfo(List<CouponAvailableInfo> availableInfo) {
            this.availableInfo = availableInfo;
            return this;
        }
        public SyncV2CouponQueryBuilder entryInfo(SpuEntryInfo entryInfo) {
            this.entryInfo = entryInfo;
            return this;
        }
        public SyncV2CouponQueryBuilder tanantId(Integer tanantId) {
            this.tanantId = tanantId;
            return this;
        }
        public SyncV2CouponQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }
        public SyncV2CouponQuery build() {
            SyncV2CouponQuery syncV2CouponQuery = new SyncV2CouponQuery();
            syncV2CouponQuery.setAttribute(attribute);
            syncV2CouponQuery.setClaim_end_time(claimEndTime);
            syncV2CouponQuery.setClaim_start_time(claimStartTime);
            syncV2CouponQuery.setExt_coupon_id(extCouponId);
            syncV2CouponQuery.setAvailable_info(availableInfo);
            syncV2CouponQuery.setEntry_info(entryInfo);
            syncV2CouponQuery.setTenantId(tanantId);
            syncV2CouponQuery.setClientKey(clientKey);
            syncV2CouponQuery.setStatus(status);
            return syncV2CouponQuery;
        }

    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public CouponAttribute getAttribute() {
        return attribute;
    }

    public void setAttribute(CouponAttribute attribute) {
        this.attribute = attribute;
    }

    public Long getClaim_end_time() {
        return claim_end_time;
    }

    public void setClaim_end_time(Long claim_end_time) {
        this.claim_end_time = claim_end_time;
    }

    public Long getClaim_start_time() {
        return claim_start_time;
    }

    public void setClaim_start_time(Long claim_start_time) {
        this.claim_start_time = claim_start_time;
    }

    public String getExt_coupon_id() {
        return ext_coupon_id;
    }

    public void setExt_coupon_id(String ext_coupon_id) {
        this.ext_coupon_id = ext_coupon_id;
    }

    public List<CouponAvailableInfo> getAvailable_info() {
        return available_info;
    }

    public void setAvailable_info(List<CouponAvailableInfo> available_info) {
        this.available_info = available_info;
    }

    public SpuEntryInfo getEntry_info() {
        return entry_info;
    }

    public void setEntry_info(SpuEntryInfo entry_info) {
        this.entry_info = entry_info;
    }
}
