package com.dyj.applet.domain.vo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-13 16:23
 **/
public class UpdateCommonPlanStatusVo {

    private List<Long> fail_plan_id_list;

    public List<Long> getFail_plan_id_list() {
        return fail_plan_id_list;
    }

    public void setFail_plan_id_list(List<Long> fail_plan_id_list) {
        this.fail_plan_id_list = fail_plan_id_list;
    }
}
