package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.UserInfoQuery;

/**
 * 查询授权用户发放的活动信息请求值
 */
public class QueryActivityMetaDetailListQuery extends UserInfoQuery {

    /**
     * 抖音开平券模板号 选填
     */
    private String coupon_meta_id;
    /**
     * 券名称，长度15以内，根据券名称模糊查询。 选填
     */
    private String coupon_name;
    /**
     * 优惠类型 选填
     */
    private Integer discount_type;

    /**
     * 页码，从1开始
     */
    private Integer page_num;
    /**
     * 单页大小，不能超过100
     */
    private Integer page_size;
    /**
     * <p>券的<strong>发放场景</strong>，仅支持4（IM场景发送）：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">取值4：IM场景发送</li></ul>
     */
    private Integer send_scene;
    /**
     * 活动状态
     */
    private Integer status;


    public String getCoupon_meta_id() {
        return coupon_meta_id;
    }

    public QueryActivityMetaDetailListQuery setCoupon_meta_id(String coupon_meta_id) {
        this.coupon_meta_id = coupon_meta_id;
        return this;
    }

    public String getCoupon_name() {
        return coupon_name;
    }

    public QueryActivityMetaDetailListQuery setCoupon_name(String coupon_name) {
        this.coupon_name = coupon_name;
        return this;
    }

    public Integer getDiscount_type() {
        return discount_type;
    }

    public QueryActivityMetaDetailListQuery setDiscount_type(Integer discount_type) {
        this.discount_type = discount_type;
        return this;
    }

    public Integer getPage_num() {
        return page_num;
    }

    public QueryActivityMetaDetailListQuery setPage_num(Integer page_num) {
        this.page_num = page_num;
        return this;
    }

    public Integer getPage_size() {
        return page_size;
    }

    public QueryActivityMetaDetailListQuery setPage_size(Integer page_size) {
        this.page_size = page_size;
        return this;
    }

    public Integer getSend_scene() {
        return send_scene;
    }

    public QueryActivityMetaDetailListQuery setSend_scene(Integer send_scene) {
        this.send_scene = send_scene;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public QueryActivityMetaDetailListQuery setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public static QueryActivityMetaDetailListQueryBuilder builder(){
        return new QueryActivityMetaDetailListQueryBuilder();
    }

    public static final class QueryActivityMetaDetailListQueryBuilder {
        private String coupon_meta_id;
        private String coupon_name;
        private Integer discount_type;
        private Integer page_num;
        private Integer page_size;
        private Integer send_scene;
        private Integer status;
        private String open_id;
        private Integer tenantId;
        private String clientKey;

        private QueryActivityMetaDetailListQueryBuilder() {
        }

        public QueryActivityMetaDetailListQueryBuilder couponMetaId(String couponMetaId) {
            this.coupon_meta_id = couponMetaId;
            return this;
        }

        public QueryActivityMetaDetailListQueryBuilder couponName(String couponName) {
            this.coupon_name = couponName;
            return this;
        }

        public QueryActivityMetaDetailListQueryBuilder discountType(Integer discountType) {
            this.discount_type = discountType;
            return this;
        }

        public QueryActivityMetaDetailListQueryBuilder pageNum(Integer pageNum) {
            this.page_num = pageNum;
            return this;
        }

        public QueryActivityMetaDetailListQueryBuilder pageSize(Integer pageSize) {
            this.page_size = pageSize;
            return this;
        }

        public QueryActivityMetaDetailListQueryBuilder sendScene(Integer sendScene) {
            this.send_scene = sendScene;
            return this;
        }

        public QueryActivityMetaDetailListQueryBuilder status(Integer status) {
            this.status = status;
            return this;
        }

        public QueryActivityMetaDetailListQueryBuilder openId(String openId) {
            this.open_id = openId;
            return this;
        }

        public QueryActivityMetaDetailListQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryActivityMetaDetailListQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryActivityMetaDetailListQuery build() {
            QueryActivityMetaDetailListQuery queryActivityMetaDetailListQuery = new QueryActivityMetaDetailListQuery();
            queryActivityMetaDetailListQuery.setCoupon_meta_id(coupon_meta_id);
            queryActivityMetaDetailListQuery.setCoupon_name(coupon_name);
            queryActivityMetaDetailListQuery.setDiscount_type(discount_type);
            queryActivityMetaDetailListQuery.setPage_num(page_num);
            queryActivityMetaDetailListQuery.setPage_size(page_size);
            queryActivityMetaDetailListQuery.setSend_scene(send_scene);
            queryActivityMetaDetailListQuery.setStatus(status);
            queryActivityMetaDetailListQuery.setOpen_id(open_id);
            queryActivityMetaDetailListQuery.setTenantId(tenantId);
            queryActivityMetaDetailListQuery.setClientKey(clientKey);
            return queryActivityMetaDetailListQuery;
        }
    }
}
