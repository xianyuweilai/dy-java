package com.dyj.applet.domain;

/**
 * 修改营销活动-复访营销活动
 */
public class ModifyPromotionActivitySidebarActivity {
    /**
     * <p><span style="color: #171A1C;"><span style="font-size: 14px;" elementtiming="element-timing">用户通过复访链路访问侧边栏时高价值区展示的内容（少于11个字）</span></span></p> 选填
     */
    private String high_value_content;
    /**
     * <p style="text-align: left;line-height: 1.38;margin: 0px 0px 0px;"><span style="color: #171A1C;"><span style="font-size: 14px;" elementtiming="element-timing">用户通过复访链路访问侧边栏时高价值区跳转按钮展示的内容，只允许选择以下内容并传入</span></span></p><ul><li style="line-height: 1.38;"><span style="color: #1C1F23;"><span style="font-size: 14px;" elementtiming="element-timing">去使用</span></span></li><li style="line-height: 1.38;"><span style="color: #1C1F23;"><span style="font-size: 14px;" elementtiming="element-timing">去领取</span></span></li><li style="line-height: 1.38;"><span style="color: #1C1F23;"><span style="font-size: 14px;" elementtiming="element-timing">免费看</span></span></li><li style="line-height: 1.38;"><span style="color: #1C1F23;"><span style="font-size: 14px;" elementtiming="element-timing">领优惠</span></span></li></ul> 选填
     */
    private String jump_text;
    /**
     * <p style="text-align: left;line-height: 1.38;margin: 0px 0px 0px;"><span style="color: #171A1C;"><span style="font-size: 14px;" elementtiming="element-timing">用户通过复访链路访问侧边栏时最近使用区小程序右上角气泡展示的内容，只允许选择以下内容并传入</span></span></p><ul><li style="line-height: 1.38;"><span style="color: #1C1F23;"><span style="font-size: 14px;" elementtiming="element-timing">有福利</span></span></li><li style="line-height: 1.38;"><span style="color: #1C1F23;"><span style="font-size: 14px;" elementtiming="element-timing">领福利</span></span></li><li style="line-height: 1.38;"><span style="color: #1C1F23;"><span style="font-size: 14px;" elementtiming="element-timing">免费看</span></span></li><li style="line-height: 1.38;"><span style="color: #1C1F23;"><span style="font-size: 14px;" elementtiming="element-timing">有优惠</span></span></li></ul> 选填
     */
    private String recent_bubble_text;
    /**
     * <p style="text-align: left;line-height: 1.38;margin: 0px 0px 0px;"><span style="color: #171A1C;"><span style="font-size: 14px;" elementtiming="element-timing">营销活动短标题（少于5个字）</span></span></p><ul><li style="line-height: 1.38;"><span style="color: #1C1F23;"><span style="font-size: 14px;" elementtiming="element-timing">用于用户端弹窗和复访时展示，可以简单强调营销内容</span></span></li><li style="line-height: 1.38;"><span style="color: #1C1F23;"><span style="font-size: 14px;" elementtiming="element-timing">需审核</span></span></li></ul> 选填
     */
    private String short_title;
    /**
     * <p style="text-align: left;line-height: 1.38;margin: 0px 0px 0px;"><span style="color: #171A1C;"><span style="font-size: 14px;" elementtiming="element-timing">营销活动生效时间（即可领取开始时间），单位秒</span></span></p><ul><li style="line-height: 1.38;"><span style="color: #1C1F23;"><span style="font-size: 14px;" elementtiming="element-timing">营销活动的声明周期必须为关联券模板生命周期的子集</span></span></li></ul> 选填
     */
    private Long valid_begin_time;
    /**
     * <p style="text-align: left;line-height: 1.38;margin: 0px 0px 0px;"><span style="color: #171A1C;"><span style="font-size: 14px;" elementtiming="element-timing">营销活动过期时间（即可领取结束时间），单位秒</span></span></p><ul><li style="line-height: 1.38;"><span style="color: #1C1F23;"><span style="font-size: 14px;" elementtiming="element-timing">营销活动的声明周期必须为关联券模板生命周期的子集</span></span></li></ul> 选填
     */
    private Long valid_end_time;

    public String getHigh_value_content() {
        return high_value_content;
    }

    public ModifyPromotionActivitySidebarActivity setHigh_value_content(String high_value_content) {
        this.high_value_content = high_value_content;
        return this;
    }

    public String getJump_text() {
        return jump_text;
    }

    public ModifyPromotionActivitySidebarActivity setJump_text(String jump_text) {
        this.jump_text = jump_text;
        return this;
    }

    public String getRecent_bubble_text() {
        return recent_bubble_text;
    }

    public ModifyPromotionActivitySidebarActivity setRecent_bubble_text(String recent_bubble_text) {
        this.recent_bubble_text = recent_bubble_text;
        return this;
    }

    public String getShort_title() {
        return short_title;
    }

    public ModifyPromotionActivitySidebarActivity setShort_title(String short_title) {
        this.short_title = short_title;
        return this;
    }

    public Long getValid_begin_time() {
        return valid_begin_time;
    }

    public ModifyPromotionActivitySidebarActivity setValid_begin_time(Long valid_begin_time) {
        this.valid_begin_time = valid_begin_time;
        return this;
    }

    public Long getValid_end_time() {
        return valid_end_time;
    }

    public ModifyPromotionActivitySidebarActivity setValid_end_time(Long valid_end_time) {
        this.valid_end_time = valid_end_time;
        return this;
    }

    public static ModifyPromotionActivitySidebarActivityBuilder builder(){
        return new ModifyPromotionActivitySidebarActivityBuilder();
    }

    public static final class ModifyPromotionActivitySidebarActivityBuilder {
        private String high_value_content;
        private String jump_text;
        private String recent_bubble_text;
        private String short_title;
        private Long valid_begin_time;
        private Long valid_end_time;

        private ModifyPromotionActivitySidebarActivityBuilder() {
        }

        public ModifyPromotionActivitySidebarActivityBuilder highValueContent(String highValueContent) {
            this.high_value_content = highValueContent;
            return this;
        }

        public ModifyPromotionActivitySidebarActivityBuilder jumpText(String jumpText) {
            this.jump_text = jumpText;
            return this;
        }

        public ModifyPromotionActivitySidebarActivityBuilder recentBubbleText(String recentBubbleText) {
            this.recent_bubble_text = recentBubbleText;
            return this;
        }

        public ModifyPromotionActivitySidebarActivityBuilder shortTitle(String shortTitle) {
            this.short_title = shortTitle;
            return this;
        }

        public ModifyPromotionActivitySidebarActivityBuilder validBeginTime(Long validBeginTime) {
            this.valid_begin_time = validBeginTime;
            return this;
        }

        public ModifyPromotionActivitySidebarActivityBuilder validEndTime(Long validEndTime) {
            this.valid_end_time = validEndTime;
            return this;
        }

        public ModifyPromotionActivitySidebarActivity build() {
            ModifyPromotionActivitySidebarActivity modifyPromotionActivitySidebarActivity = new ModifyPromotionActivitySidebarActivity();
            modifyPromotionActivitySidebarActivity.setHigh_value_content(high_value_content);
            modifyPromotionActivitySidebarActivity.setJump_text(jump_text);
            modifyPromotionActivitySidebarActivity.setRecent_bubble_text(recent_bubble_text);
            modifyPromotionActivitySidebarActivity.setShort_title(short_title);
            modifyPromotionActivitySidebarActivity.setValid_begin_time(valid_begin_time);
            modifyPromotionActivitySidebarActivity.setValid_end_time(valid_end_time);
            return modifyPromotionActivitySidebarActivity;
        }
    }
}
