package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.QueryCouponMeta;

/**
 * 查询券模板返回值
 */
public class QueryCouponMetaVo {
    /**
     * <p>err_no非0时对应的错误信息文案提示，成功时为空字符串</p>
     */
    private String err_msg;
    /**
     * <p>0表示成功</p>
     */
    private Integer err_no;
    /**
     * <p>标识请求的唯一ID</p>
     */
    private String log_id;
    /**
     *
     */
    private QueryCouponMeta coupon_meta;


    public String getErr_msg() {
        return err_msg;
    }

    public QueryCouponMetaVo setErr_msg(String err_msg) {
        this.err_msg = err_msg;
        return this;
    }

    public Integer getErr_no() {
        return err_no;
    }

    public QueryCouponMetaVo setErr_no(Integer err_no) {
        this.err_no = err_no;
        return this;
    }

    public String getLog_id() {
        return log_id;
    }

    public QueryCouponMetaVo setLog_id(String log_id) {
        this.log_id = log_id;
        return this;
    }

    public QueryCouponMeta getCoupon_meta() {
        return coupon_meta;
    }

    public QueryCouponMetaVo setCoupon_meta(QueryCouponMeta coupon_meta) {
        this.coupon_meta = coupon_meta;
        return this;
    }
}
